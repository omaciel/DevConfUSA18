.. Web UI Automation with Selenium for Beginners documentation master file, created by
   sphinx-quickstart on Sat Jul 28 13:56:16 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Web UI Automation with Selenium for Beginners
=============================================

.. figure:: images/webautomation.png
   :figclass: align-right

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   biography
   objective
   followalong
   definitions
   selenium
   seleniumide
   sciroccoide
   seleniumwithpython
   pythonseleniumwithunittest
   pythonseleniumwithpytest
   pythonseleniumwithsaucelabs
   gitlab


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
