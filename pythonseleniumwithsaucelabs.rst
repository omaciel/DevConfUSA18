###############################################
Using Python Selenium with Pytest and SauceLabs
###############################################
Assuming that you have an account at `SauceLabs`_ and that you have
exported your credentials via your system's environmental variables,
let's re-create those same automated tests and execute them on
**SauceLabs**:

.. include:: usecases.rst


.. _SauceLabs: https://saucelabs.com/

-----------
Python Code
-----------
Here is the Python code that will automate these tests:

++++++++
Fixtures
++++++++

.. literalinclude:: tests/saucelabs/conftest.py
   :language: Python
   :linenos:
   :emphasize-lines: 66-86

+++++
Tests
+++++

.. literalinclude:: tests/saucelabs/test_SeleniumSauce.py
   :language: Python
   :linenos:

Now we can execute these tests:

.. code-block:: shell

    pytest -v -n 2 --driver SauceLabs tests/saucelabs/test_SeleniumSauce.py -k test_page_title
    ============================================================================================= test session starts =============================================================================================
    platform darwin -- Python 3.7.0, pytest-3.6.4, py-1.5.4, pluggy-0.6.0 -- /Users/omaciel/.virtualenvs/devconfusa18/bin/python
    cachedir: .pytest_cache
    driver: SauceLabs
    sensitiveurl: .*
    metadata: {'Python': '3.7.0', 'Platform': 'Darwin-17.7.0-x86_64-i386-64bit', 'Packages': {'pytest': '3.6.4', 'py': '1.5.4', 'pluggy': '0.6.0'}, 'Plugins': {'xdist': '1.22.5', 'variables': '1.7.1', 'selenium': '1.13.0', 'metadata': '1.7.0', 'html': '1.19.0', 'forked': '0.2', 'base-url': '1.4.1'}, 'Base URL': '', 'Driver': 'SauceLabs', 'Capabilities': {}}
    rootdir: /Users/omaciel/hacking/devconfusa18, inifile:
    plugins: xdist-1.22.5, variables-1.7.1, selenium-1.13.0, metadata-1.7.0, html-1.19.0, forked-0.2, base-url-1.4.1
    [gw0] darwin Python 3.7.0 cwd: /Users/omaciel/hacking/devconfusa18
    [gw1] darwin Python 3.7.0 cwd: /Users/omaciel/hacking/devconfusa18
    [gw0] Python 3.7.0 (default, Jun 29 2018, 20:13:13)  -- [Clang 9.1.0 (clang-902.0.39.2)]
    [gw1] Python 3.7.0 (default, Jun 29 2018, 20:13:13)  -- [Clang 9.1.0 (clang-902.0.39.2)]
    gw0 [4] / gw1 [4]
    scheduling tests via LoadScheduling

    tests/saucelabs/test_SeleniumSauce.py::test_page_title[chrome]
    tests/saucelabs/test_SeleniumSauce.py::test_page_title[MicrosoftEdge]
    [gw0] [ 25%] PASSED tests/saucelabs/test_SeleniumSauce.py::test_page_title[chrome]
    tests/saucelabs/test_SeleniumSauce.py::test_page_title[firefox]
    [gw0] [ 50%] PASSED tests/saucelabs/test_SeleniumSauce.py::test_page_title[firefox]
    [gw1] [ 75%] PASSED tests/saucelabs/test_SeleniumSauce.py::test_page_title[MicrosoftEdge]
    tests/saucelabs/test_SeleniumSauce.py::test_page_title[safari]
    [gw1] [100%] PASSED tests/saucelabs/test_SeleniumSauce.py::test_page_title[safari]

    ========================================================================================== 4 passed in 63.22 seconds ==========================================================================================


.. image:: images/saucelabs-dashboard.png
