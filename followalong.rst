############
Follow Along
############
You can get a copy of all files used in this tutorial by cloning
this repository!

.. code-block:: shell

    git clone https://gitlab.com/omaciel/DevConfUSA18.git

Then, make sure to install all the required Python modules, either
using `pip`...

.. code-block:: shell

    pip install -r requirements.txt

... or the `make` command:

.. code-block:: shell

    make install
