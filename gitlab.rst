################
Gitlab Pipelines
################
Now that we have successfully automated a couple of Use Cases, how about
creating an automated process that can handle re-running our tests whenever
changes to the source code happens?

For the remainder of this presentation I will cover how we can leverage
Gitlab's Pipelines to perform the following actions:

* Rebuild this presentation
* Execute our automated tests via pytest against

  * Firefox
  * Google Chrome

* Lastly, assuming our tests pass, re-run them against a matrix of web browsers
  and operating systems on SauceLabs

All of these actions will be performed by our Pipeline automatically and
without requiring any infrastructure from you, every time code changes get
merged into this repository.

---------
Pipelines
---------
"A pipeline is a group of jobs that get executed in stages(batches). All of
the jobs in a stage are executed in parallel (if there are enough concurrent
Runners), and if they all succeed, the pipeline moves on to the next stage.
If one of the jobs fails, the next stage is not (usually) executed. You can
access the pipelines page in your project's Pipelines tab. [#]_"

------------------
Defining Pipelines
------------------
a pipeline on Gitlab is defined by adding a `.gitlab-ci.yml` file to your
repository. The pipeline for this repository can be seen bellow:

.. literalinclude:: .gitlab-ci.yml
   :language: yaml
   :linenos:

There are a total of 4 unique jobs:

* Build Document: executes `make html` and builds the document for this
  presentation
* UI Pytest Chrome: Runs automated tests against a Dockerized instance of
  Google Chrome
* UI Pytest Firefox: Runs a subset of automated tests against a Dockerized
  instance of Firefox
* UI Pytest SauceLabs Matrix: Runs a subset of automated tests against a
  matrix of web browsers and operating systems on SauceLabs

Furthermore, these jobs are executed by stages, each stage being a
requirement for the next one, as shown below:

* Generate Docs
* Test Pytest
* Test SauceLabs

.. image:: images/gitlab-pipelines.png

.. NOTE:: In order to run our automated tests against `SauceLabs`, we need
   to define to environmental variables that will grant the automation
   access to SauceLabs environment:

   * SAUCELABS_API_KEY
   * SAUCELABS_USERNAME

   .. image:: images/saucelabs-variables.png

   Since we don't want to keep this information accessible to the WWW,
   Gitlab offers a convenient `Variables`_ page that let's you store
   per-project or per-group variables which can then be accessed by your
   jobs at runtime. Furthermore, you can mark them as **proctected** so
   that they won't be displayed anywhere on the UI or logs!

Though I won't go into the steps required to create jobs or the syntax,
this `document`_ is a very good start. The `.gitlab-ci.yml` file
provided here should also provide you with the 'seed' for your onw file.

.. [#] Pipelines https://docs.gitlab.com/ee/ci/pipelines.html
.. _Variables: https://gitlab.com/help/ci/variables/README#variables
.. _document: https://docs.gitlab.com/ee/ci/yaml/README.html#jobs
