#################
About the Speaker
#################

.. figure:: images/ogmaciel.png
   :scale: 50 %
   :alt: Og Maciel
   :align: center
   :figclass: align-right

   Og Maciel

Og Maciel is a Senior Manager of Quality Engineering at
**Red Hat**. He has spent the last 6+ years building a team
of **Black Belt Quality Engineers** responsible for the
automation of complex systems and delivering quality
products through the use of Continuous Delivery of
Processes. He is also an Avid Reader.

* Projects

    * Red Hat Satellite
    * Pulp Project
    * Ansible Tower

* Social

    * `LinkedIn`_
    * `Gitlab`_
    * `Github`_
    * `Twitter`_
    * `GoodReads`_

.. _LinkedIn: https://www.linkedin.com/in/ogmaciel/
.. _Gitlab: https://gitlab.com/omaciel
.. _Github: https://github.com/omaciel
.. _Twitter: https://twitter.com/OgMaciel
.. _GoodReads: https://www.goodreads.com/omaciel
