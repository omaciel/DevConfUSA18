#########
Objective
#########

* Cover **Web UI automation** using **Selenium** with a focus on the
  **Python** programming language.
* Learn how to easily gather Web UI information, record their
  actions and play them back via **Selenium IDE** or **Scirocco**.
* Write **Python** code to perform the same actions **interactively**
* Write automated tests using Python's **Unittest** module 'style'
* Write automated tests using **pytest** 'style'
* Use **SauceLabs** to execute automated tests on multiple types
  of operating systems and web browser combinations.
* **BONUS Round**: Time permiting, use **Gitlab Pipelines** for
  a **Continuous Integration / Delivery** process
