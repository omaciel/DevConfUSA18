=========================
Selenium IDE: Simple Test
=========================
Here's an example of *Googling* for the term **Red Hat**
and clicking the corresponding link.

.. image:: images/selenium_ide.png

This test can also be exported as **JSON** and be opened at a later time

.. code-block:: JSON

    {
    "id": "b797dc14-e81e-4869-952f-678661776c02",
    "name": "Untitled Project",
    "url": "https://www.google.com",
    "tests": [{
        "id": "c73764c1-67ef-477f-991a-35ff97a0652b",
        "name": "Untitled",
        "commands": [{
        "id": "59e5e648-0b92-4480-99ac-eeb3ba456433",
        "comment": "",
        "command": "open",
        "target": "/",
        "value": ""
        }, {
        "id": "083c23db-032b-4495-9947-b0668924d4a1",
        "comment": "",
        "command": "clickAt",
        "target": "id=lst-ib",
        "value": "16,16"
        }, {
        "id": "0c8489a7-9b1c-40ba-9967-3b79fc3e7107",
        "comment": "",
        "command": "clickAt",
        "target": "css=h3.r > a",
        "value": "19,15"
        }]
    }],
    "suites": [{
        "id": "22654ef7-6e89-46e9-bd19-247030d0db7d",
        "name": "Default Suite",
        "parallel": false,
        "timeout": 300,
        "tests": ["c73764c1-67ef-477f-991a-35ff97a0652b"]
    }],
    "urls": ["https://www.google.com/"],
    "plugins": [],
    "version": "1.0"
    }
