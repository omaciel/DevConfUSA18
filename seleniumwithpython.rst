##########################
Using Selenium with Python
##########################
Now we will write actual **Python** code to interact with a web browser.

------------------------------
Install Selenium Python module
------------------------------
The `selenium` module provides several useful methods that lets you
browser web pages and interact with their web elements.

.. figure:: images/selenium-cheatsheet.png
   :alt: cheatsheet
   :target: https://duckduckgo.com/?q=selenium+commands&atb=v101-7&ia=cheatsheet&iax=1


Some examples:

* find_element_by_id
* find_element_by_class_name
* find_element_by_tag_name
* find_element_by_css_selector
* find_element_by_partial_link_text
* find_element_by_xpath

For the following HTML fragment:

.. code:: html

   <div id="coolestWidgetEvah">...</div>
   <div class="cheese"><span>Cheddar</span></div><div class="cheese"><span>Gouda</span></div>
   <iframe src="..."></iframe>
   <div id="food"><span class="dairy">milk</span><span class="dairy aged">cheese</span></div>
   <a href="http://www.google.com/search?q=cheese">search for cheese</a>
   <input type="text" name="example" />

We can use Selenium to locate several different elements:

.. code:: python

   element = driver.find_element_by_id("coolestWidgetEvah")
   cheeses = driver.find_elements_by_class_name("cheese")
   frame = driver.find_element_by_tag_name("iframe")
   cheese = driver.find_element_by_css_selector("#food span.dairy.aged")
   cheese = driver.find_element_by_partial_link_text("cheese")
   inputs = driver.find_elements_by_xpath("//input")

For further information about the many more useful methods provided by Selenium, please
refer to the `documentation <https://seleniumhq.github.io/selenium/docs/api/py/webdriver_remote/selenium.webdriver.remote.webdriver.html>`_.

So let's go ahead and install the `selenium` module:

.. NOTE:: This step can be skipped if you've cloned the repository
   and installed all Python dependencies.

.. code-block:: shell

    pip install selenium

--------------------
Install a Web Driver
--------------------
Selenium requires a driver to interface with the chosen browser. Firefox, for
example, requires geckodriver Some of the more popular ones are:


.. list-table::
   :widths: 15 30
   :header-rows: 1

   * - Web browser
     - Download URL
   * - Google Chrome
     - https://sites.google.com/a/chromium.org/chromedriver/downloads
   * - Microsoft Edge
     - https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/
   * - Firefox
     -	https://github.com/mozilla/geckodriver/releases
   * - Safari
     - https://webkit.org/blog/6900/webdriver-support-in-safari-10/


.. NOTE:: For this tutorial I have chosen to use the web driver for
   the Chrome web browser. On a Mac I can easily install it with:

   .. code-block:: shell

     brew cask install chromedriver


.. NOTE:: Make sure to include the ChromeDriver location in your PATH
   environment variable

----------------------------------------
Interact with Chrome via Python Selenium
----------------------------------------
Open a Python shell and type the code below to search for
Red Hat via Google interactively:

.. literalinclude:: python/interactive.py
   :language: Python
   :linenos:
