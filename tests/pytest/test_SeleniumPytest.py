from selenium.webdriver.common.keys import Keys


def test_page_title(selenium):
    selenium.get('https://www.google.com')
    assert 'Google' in selenium.title


def test_search_page_title(selenium):
    selenium.implicitly_wait(10)
    selenium.get('https://www.google.com')
    assert 'Google' in selenium.title
    element = selenium.find_element_by_id('lst-ib')
    assert element is not None
    element.send_keys('Red Hat' + Keys.RETURN)
    assert selenium.title.startswith('Red Hat')
