###################################
Using Python Selenium with Unittest
###################################
Let's create a couple of automated tests using Python's
**unittest** module:

.. include:: usecases.rst

-----------
Python Code
-----------
Here is the Python code that will automate these tests:

.. literalinclude:: tests/unittest/test_SeleniumUnittest.py
   :language: Python
   :linenos:

Now we can execute these tests:

.. code-block:: shell

    python tests/unittest/test_SeleniumUnittest.py
    test_page_title (__main__.GoogleTestCase)
    Assert that title of page says 'Google'. ... ok
    test_search_page_title (__main__.GoogleTestCase)
    Assert that Google search returns data for 'Red Hat'. ... ok

    ----------------------------------------------------------------------
    Ran 2 tests in 7.765s

    OK
