---------
Use Cases
---------

* As a user, when I open www.google.com on my web browser,
  the web browser tab will show the word **Google** as its
  title
* As a user, when I open www.google.com on my web browser
  and search for the word **Red Hat**, the web browser tab
  will show the word **Red Hat** as its title
