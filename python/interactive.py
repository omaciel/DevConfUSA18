from selenium import webdriver
from selenium.webdriver.common.keys import Keys


# Start Google Chrome
browser = webdriver.Chrome()

# Open Google.com
browser.get('https://www.google.com/')

# Locate the search box
element = browser.find_element_by_id('lst-ib')
assert element is not None

# Search for Red Hat
element.send_keys('Red Hat' + Keys.RETURN)
assert browser.title.startswith('Red Hat')

# Visit Google.com
browser.get('https://www.google.com')

# Locate the Google Logo
element = browser.find_element_by_id('hplogo')
assert element is not None

# Change the logo and adjust its height
browser.execute_script(
    "arguments[0].setAttribute('srcset', "
    "'https://omaciel.fedorapeople.org/ogmaciel.png')",
    element
    )
browser.execute_script(
    "arguments[0].setAttribute('height', '100%')",
    element
    )

# Close the browser
browser.quit()
