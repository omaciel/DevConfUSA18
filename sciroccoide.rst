#####################
Scirocco Recorder IDE
#####################

========
Overview
========
* Records all interactions with browser
* Can replay all recordings
* **Not** as full fledged IDE as Selenium IDE
* Limited range of commands
* Limited options for exporting test cases (Python is NOT supported)

=================
How to install it
=================
* Make sure that you're using an updated version of Chrome
* Install `Scirocco Recorder`_ for Chrome

.. image:: images/scirocco_ide.png

.. _Scirocco Recorder: https://chrome.google.com/webstore/detail/scirocco-recorder-for-chr/ibclajljffeaafooicpmkcjdnkbaoiih
