#################################
Using Python Selenium with pytest
#################################
Let's re-create those same automated tests but now using
pytest:

.. include:: usecases.rst

-----------------------
Install python-selenium
-----------------------
.. NOTE:: This step can be skipped if you've cloned the repository
   and installed all Python dependencies.

pytest-selenium is a plugin for pytest that provides support for
running Selenium based tests by providing a `selenium` fixture.
You can find more information about this plugin by reading the
official `pytest-selenium`_ documentation.

The most important reason for using it though is that through its
`selenium` fixture we get automatically a `setup` and `teardown`
without the need to write a single line of code (which can be
overriden if needed of course!).

.. code-block:: shell

    pip install python-selenium

-----------
Python Code
-----------
Here is the Python code that will automate these tests:

.. literalinclude:: tests/pytest/test_SeleniumPytest.py
   :language: Python
   :linenos:

Now we can execute these tests:

.. code-block:: shell

    pytest -v --driver chrome tests/pytest/test_SeleniumPytest.py
    ============================================================================================= test session starts =============================================================================================
    platform darwin -- Python 3.7.0, pytest-3.6.4, py-1.5.4, pluggy-0.6.0 -- /Users/omaciel/.virtualenvs/devconfusa18/bin/python
    cachedir: .pytest_cache
    driver: chrome
    sensitiveurl: .*
    metadata: {'Python': '3.7.0', 'Platform': 'Darwin-17.7.0-x86_64-i386-64bit', 'Packages': {'pytest': '3.6.4', 'py': '1.5.4', 'pluggy': '0.6.0'}, 'Plugins': {'xdist': '1.22.5', 'variables': '1.7.1', 'selenium': '1.13.0', 'metadata': '1.7.0', 'html': '1.19.0', 'forked': '0.2', 'base-url': '1.4.1'}, 'Base URL': '', 'Driver': 'chrome', 'Capabilities': {}}
    rootdir: /Users/omaciel/hacking/devconfusa18, inifile:
    plugins: xdist-1.22.5, variables-1.7.1, selenium-1.13.0, metadata-1.7.0, html-1.19.0, forked-0.2, base-url-1.4.1
    collected 2 items

    tests/pytest/test_SeleniumPytest.py::test_page_title PASSED                                                                                                                                             [ 50%]
    tests/pytest/test_SeleniumPytest.py::test_search_page_title PASSED                                                                                                                                      [100%]

    ========================================================================================== 2 passed in 6.51 seconds ===========================================================================================

--------------------------------
Running All Tests Simultaneously
--------------------------------
Now, let's execute all tests in multiple threads:

++++++++++++++++++++
Install python-xdist
++++++++++++++++++++
.. NOTE:: This step can be skipped if you've cloned the repository
   and installed all Python dependencies.

.. code-block:: shell

    pip install python-xdist

+++++++++++++++++
Execute All Tests
+++++++++++++++++

Run the following command:

.. code-block:: shell

    pytest -v -n 2 --driver chrome tests/pytest/test_SeleniumPytest.py
    ============================================================================================= test session starts =============================================================================================
    platform darwin -- Python 3.7.0, pytest-3.6.4, py-1.5.4, pluggy-0.6.0 -- /Users/omaciel/.virtualenvs/devconfusa18/bin/python
    cachedir: .pytest_cache
    driver: chrome
    sensitiveurl: .*
    metadata: {'Python': '3.7.0', 'Platform': 'Darwin-17.7.0-x86_64-i386-64bit', 'Packages': {'pytest': '3.6.4', 'py': '1.5.4', 'pluggy': '0.6.0'}, 'Plugins': {'xdist': '1.22.5', 'variables': '1.7.1', 'selenium': '1.13.0', 'metadata': '1.7.0', 'html': '1.19.0', 'forked': '0.2', 'base-url': '1.4.1'}, 'Base URL': '', 'Driver': 'chrome', 'Capabilities': {}}
    rootdir: /Users/omaciel/hacking/devconfusa18, inifile:
    plugins: xdist-1.22.5, variables-1.7.1, selenium-1.13.0, metadata-1.7.0, html-1.19.0, forked-0.2, base-url-1.4.1
    [gw0] darwin Python 3.7.0 cwd: /Users/omaciel/hacking/devconfusa18
    [gw1] darwin Python 3.7.0 cwd: /Users/omaciel/hacking/devconfusa18
    [gw0] Python 3.7.0 (default, Jun 29 2018, 20:13:13)  -- [Clang 9.1.0 (clang-902.0.39.2)]
    [gw1] Python 3.7.0 (default, Jun 29 2018, 20:13:13)  -- [Clang 9.1.0 (clang-902.0.39.2)]
    gw0 [2] / gw1 [2]
    scheduling tests via LoadScheduling

    tests/pytest/test_SeleniumPytest.py::test_search_page_title
    tests/pytest/test_SeleniumPytest.py::test_page_title
    [gw0] [ 50%] PASSED tests/pytest/test_SeleniumPytest.py::test_page_title
    [gw1] [100%] PASSED tests/pytest/test_SeleniumPytest.py::test_search_page_title

    ========================================================================================== 2 passed in 5.19 seconds ===========================================================================================

.. _pytest-selenium: https://pytest-selenium.readthedocs.io/en/latest/index.html
