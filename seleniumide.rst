##################
Using Selenium IDE
##################

========
Overview
========
* Support for **Firefox** and **Chrome**

  * Requires Firefox == 61 or newer

* Records all interactions with browser
* Can replay all recordings
* Allows for multiple tests to be recorded
* **Used** to have tons of useful features but newer version
  has dropped most of them
* Older versions of **Selenium IDE** used to support exporting
  your tests to several formats, including **Python**

  * You could run the exported Python code and be done with it!

.. image:: images/selenium_ide_export_python.png

* Now only **JSON** format seems to be supported

.. image:: images/selenium_ide_export_json.png

* Requires creativity to ensure that playing back actions
  will wait for web elements to be present

=================
How to install it
=================

-------
Firefox
-------
* Make sure that you're using an updated version of Firefox.

  * I recommend Firefox `Quantum`_ version 61 or greater
  * You **can** have multiple versions running side by side; just
    rename the executable to **Firefox45** for example

.. important:: If using an older version of **Firefox**, in order to
   preventing it from updating itself, you'll have to disable the
   automatic update feature. To do that:

   - Immediately after starting Firefox, choose the **Preferences** menu
   - Check the *Never check for updates (not recommended: security risk)*
     option

   .. image:: images/firefox-preferences.png

* Install `Selenium IDE for Firefox`_

-------------
Google Chrome
-------------
* Make sure that you're using an updated version of Chrome
* Install `Selenium IDE for Chrome`_

====
Demo
====

.. include:: simpletestide.rst
.. include:: bettertestide.rst


.. _Quantum: https://ftp.mozilla.org/pub/firefox/releases/61.0.1/
.. _Selenium IDE for Firefox: https://addons.mozilla.org/en-US/firefox/addon/selenium-ide/
.. _Selenium IDE for Chrome: https://chrome.google.com/webstore/detail/selenium-ide/mooikfkahbdckldjjndioackbalphokd
.. _Selenium IDE demo: https://youtu.be/e1jrrg39V_k

