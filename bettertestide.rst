=========================
Selenium IDE: Better Test
=========================
Here's the same example of *Googling* for the term **Red Hat**
and clicking the corresponding link, but we're explicitly
setting the text that must be clicked on

.. image:: images/selenium_ide_better.png

This test can also be exported as **JSON** and be opened at a later time

.. code-block:: JSON

    {
    "id": "51a034da-3929-4bca-8581-3869764da024",
    "name": "Untitled Project",
    "url": "https://www.google.com",
    "tests": [{
        "id": "c5b80fac-3d33-4201-aa2e-28caa41f8f3b",
        "name": "Untitled",
        "commands": [{
        "id": "77f5e1a1-9363-4592-9cb2-a9c0dd67c520",
        "comment": "",
        "command": "open",
        "target": "/",
        "value": ""
        }, {
        "id": "22c0756b-ce6c-43af-bc12-1cb94bc85034",
        "comment": "",
        "command": "type",
        "target": "id=lst-ib",
        "value": "Red Hat"
        }, {
        "id": "a6368f72-3ccf-4592-96c8-68348739a453",
        "comment": "",
        "command": "sendKeys",
        "target": "id=lst-ib",
        "value": "${KEY_ENTER}"
        }, {
        "id": "c6b1c334-30b7-482f-a448-41eec304b504",
        "comment": "",
        "command": "assertText",
        "target": "css=h3.r > a",
        "value": "Red Hat - We make open source technologies for the enterprise"
        }, {
        "id": "0f0984fa-3a1c-47d5-80e1-275019e08ac1",
        "comment": "",
        "command": "clickAt",
        "target": "css=h3.r > a",
        "value": "Red Hat - We make open source technologies for the enterprise"
        }]
    }],
    "suites": [{
        "id": "23b8c9dd-03d5-4434-9646-21da2f664edd",
        "name": "Default Suite",
        "parallel": false,
        "timeout": 300,
        "tests": ["c5b80fac-3d33-4201-aa2e-28caa41f8f3b"]
    }],
    "urls": ["https://www.google.com/"],
    "plugins": [],
    "version": "1.0"
    }
