:orphan:

=============================================
Web UI Automation with Selenium for Beginners
=============================================

Welcome to my talk **Foo**! Here you will find the source
code for all of my examples and the documentation that will
be used for the presentation part of this talk.

The latest version for this talk can be found at:

* https://devconfusa18.readthedocs.io/en/latest/

You can also rebuild it locally by first:

* Install the required Python dependencies:

.. code:: shell

    make install

* Generate the documentation:

.. code:: shell

    make html
