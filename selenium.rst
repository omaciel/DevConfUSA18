========
Selenium
========
* Selenium is a portable software-testing framework for web applications.
* It provides a test domain-specific language (Selenese) to write tests
  in a number of popular programming languages, including:

    * C#
    * Groovy
    * Java
    * Perl
    * PHP
    * Python
    * Ruby
    * Scala

* Selenium deploys on Windows, Linux, and MacOS platforms.
* It is open-source software, released under the Apache 2.0 license [#]_
* Seems to dominate the area for web UI automation
* Web 'drivers' let's you programmatically control different types
  of web browsers

  * Firefox
  * Chrome
  * Internet Explorer or Edge
  * Safari
  * HtmlUnit (headless browser)

* *Selenese* let's you interact with a web page's elements

  * Click
  * Select
  * Type
  * Mouse movement, hovering, dragging
  * ...


.. [#] Selenium https://en.wikipedia.org/wiki/Selenium_(software)
